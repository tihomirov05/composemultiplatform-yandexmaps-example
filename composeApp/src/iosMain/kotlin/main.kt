import androidx.compose.ui.window.ComposeUIViewController
import platform.UIKit.UIViewController
import ru.gruzivezi.shared.App

fun MainViewController(systemAppearance: (isLight: Boolean) -> Unit): UIViewController {
    return ComposeUIViewController { App(systemAppearance) }
}
