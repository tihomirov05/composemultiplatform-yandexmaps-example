package ru.gruzivezi.shared

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.interop.UIKitView
import kotlinx.cinterop.ExperimentalForeignApi
import cocoapods.Alamofire.*
import cocoapods.YandexMapsMobile.*


actual fun initMap() {
    YMKMapKit.setApiKey("KEY")
    YMKMapKit.sharedInstance().onStart()
}


@OptIn(ExperimentalForeignApi::class)
@Composable
actual fun Map() {
    YMKMapKit.setApiKey("KEY")
    YMKMapKit.sharedInstance().onStart()

    UIKitView(
        modifier = Modifier.fillMaxSize(),
        factory = {
            val map = YMKMapView()
            map
        },
    )
}