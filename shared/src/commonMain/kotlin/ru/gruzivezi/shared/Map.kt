package ru.gruzivezi.shared

import androidx.compose.runtime.Composable

expect fun initMap()


@Composable
expect fun Map()